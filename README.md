# H5000 Recorder
Records timeseries data from H5000

## Setup
Install dependencies

```
pip install -r requirements.txt
```

## Run
A sample server has been provided

```bash
$ python mock-server.py
```

```bash
$ python
>>> import h5000
>>> h5000.start()
```

Navigate to [http://localhost:8087](http://localhost:8087)