from fiftycuatro_websockets import WebSocketServer, WebSocketConnection
import time, threading
import random
import json

class H5000Server(WebSocketConnection):
    def __init__(self, client, datums=[]):
        super(H5000Server, self).__init__(client)
        self.datums = datums

    def randomDatumValue(self, datum_id):
        value = random.uniform(0, 1000)
        return dict(damped=False, dampedVal=random.uniform(0,1000), id=datum_id, val=value, valStr=str(value), valid=True)

    def run(self):
        while self.running:
            print "Sending %d Message" % len(self.datums)
            time.sleep(1.0)
            data = dict(Data=[self.randomDatumValue(d) for d in self.datums]) 
            self.send(json.dumps(data))

    def onOpen(self):
        print "Open"
        self.running = True
        self.t = threading.Thread(target=self.run)
        self.t.daemon = True
        self.t.start()

    def onMessage(self, message):
       requests = json.loads(message)
       if 'DataReq' in requests:
           for request in requests['DataReq']:
               if request['id'] not in self.datums:
                   print "Adding %d Datum" % request['id']
                   self.datums.append(request['id'])


    def onClose(self):
        print "Close"
        self.running = False

def factory(client, address):
    return H5000Server(client)

def runserver():
    ws = WebSocketServer()
    ws.factory = factory
    ws.runForever()

if __name__ == "__main__":
    runserver()
