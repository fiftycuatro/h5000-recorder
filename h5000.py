import websocket
import json
import Queue
import database
import threading
import datetime
import time
from fiftycuatro_websockets import WebSocketServer, WebSocketConnection
import dateutil.parser
from SimpleHTTPServer import SimpleHTTPRequestHandler
from BaseHTTPServer import HTTPServer

WriteQ = Queue.Queue()
ResponseQ = Queue.Queue()

def load_datums_config():
    with open('datums.json') as f:
        print "Loaded"
        data = json.loads(f.read())
        print data
        return data;
        
def store_datum(datum):
    if datum['valid']:
        WriteQ.put(dict(cmd='StoreDatum', datum=datum)) 

def load_json(message):
    try    : return json.loads(message)
    except : return {}


def writeq_processor():
    while True:
        cmd = WriteQ.get()
        cmd_type = cmd['cmd']
        if   "Exit"       == cmd_type : return
        elif "StoreDatum" == cmd_type : database.append_timeseries(cmd['datum']['id'], cmd['datum']['val'])
        elif "Request"    == cmd_type :
            data = database.timeseries_range(cmd['id'], cmd['start'], cmd['end'])
            ResponseQ.put(dict(cmd="Response", id=cmd['id'], data=data, client_id=cmd['client_id']))

def responseq_processor(server):
    while True:
        cmd = ResponseQ.get()
        cmd_type = cmd['cmd']

        if   "Exit"     == cmd_type : return
        elif "Response" == cmd_type : 
            msg = json.dumps(cmd)
            [c.ws_conn.send(msg) for c in server.clients.values()]

def on_message(ws, message):
    msg = load_json(message)

    if 'Data' in msg:
        [store_datum(datum) for datum in msg['Data']]

def on_open(ws):
    datum_ids = [d['value'] for d in load_datums_config()]
    objs = [dict(id=id, repeat=True) for id in datum_ids]
    print "Sending Requests for %r" % objs
    ws.send(json.dumps(dict(DataReq=objs)))
    
def reconnect_on_close():
    while True:
        ws = websocket.WebSocketApp("ws://localhost:4567", on_open=on_open, on_message=on_message)
        ws.run_forever()
        time.sleep(1.0)
        print "Trying to Reconnect"

class QueryWebSocketConnection(WebSocketConnection):
    def onMessage(self, msg):
        msg = json.loads(msg)
        if 'Request' in msg:
            print "Sending Request"
            msg = msg['Request']
            start = dateutil.parser.parse(msg['start'])
            end = dateutil.parser.parse(msg['end'])
            id = msg['id']
            client_id=msg['client_id']
            WriteQ.put(dict(cmd="Request", id=id, start=start, end=end, client_id=client_id))

def factory(client, address):
    return QueryWebSocketConnection(client)

def start(daemon=True):

    queryServer = WebSocketServer(host='0.0.0.0', port=5678)
    queryServer.factory = factory
    queryServer_thread  = threading.Thread(target=queryServer.runForever)
    queryServer_thread.daemon = daemon
    queryServer_thread.start()

    writeq_thread = threading.Thread(target=writeq_processor)
    writeq_thread.daemon = True
    writeq_thread.start()
    
    readq_thread = threading.Thread(target=responseq_processor, args=[queryServer])
    readq_thread.daemon = True
    readq_thread.start()
   
    ws_thread = threading.Thread(target=reconnect_on_close)
    ws_thread.daemon = True
    ws_thread.start()

    httpServer = HTTPServer(('', 8087), SimpleHTTPRequestHandler)
    httpServer_thread = threading.Thread(target=httpServer.serve_forever)
    httpServer_thread.daemon = True
    httpServer_thread.start()

def query(id, minutes=1):
    end = datetime.datetime.now()
    start = end - datetime.timedelta(minutes=minutes)
    WriteQ.put(dict(cmd="Request", id=id, start=start, end=end)) 


if __name__ == "__main__":
    start(False)
        
