import sqlite3
import os
import stat
import datetime

def date_as_string(dt):
    return str(dt.isoformat())

def get_db_filename(id):
    if not os.path.exists("db"):
        os.makedirs("db")
    return "db/h5000_id-%s.db" % id

def setup_blank_tables(dbfilename, setupcodelist):
    if os.path.isfile(dbfilename) : return  # File exists! Delete if you want clean

    with sqlite3.connect(dbfilename) as conn:
        for setuptable in setupcodelist:
            conn.execute(setuptable)
            conn.commit()

    # o+rwx, g+r, o+r (744)
    os.chmod(dbfilename, stat.S_IRWXU | stat.S_IRGRP | stat.S_IROTH)


def get_db_connsql3_for(id):
    dbfilename = get_db_filename(id)
    try:
        conn = sqlite3.connect(dbfilename, timeout=30)
    except:
        error_msg = "Couldn't connect to database specified as %s resolved to %s" % (dbfilename, dbfilename)
        raise Exception(error_msg)
    return conn

class connection(object):

    def __init__(self, id):
        self.conn=get_db_connsql3_for(id)
    
    def close(self):
        self.conn.close()

    def write(self, sqltext, argtuplelist=[]):
        if len(argtuplelist)==0:
            self.conn.execute(sqltext)
        else:
            self.conn.execute(sqltext, argtuplelist)
        self.conn.commit()
    
    def read(self, sqltext, argtuplelist=[]):
        self.conn.row_factory=sqlite3.Row
        if len(argtuplelist)==0:
            ans=self.conn.execute(sqltext)
        else:
            ans=self.conn.execute(sqltext, argtuplelist)
        ans=ans.fetchall()
        return ans

# Store each connection in dict       
DB_INDEX = dict()

def setup_h5000_schema(id):
    dbfilename = get_db_filename(id)
    setup_blank_tables(dbfilename, ["CREATE TABLE timeseries (datetime text, value float)"])

def get_db_connection(id):
    if id not in DB_INDEX:
	setup_h5000_schema(id)
        DB_INDEX[id] = connection(id)

    return DB_INDEX[id]

def append_timeseries(id, value):
    conn = get_db_connection(id)
    time = date_as_string(datetime.datetime.utcnow())
    conn.write("INSERT INTO timeseries VALUES (?, ?)", (time, value))
    #print "TS[%s], ID[%d], VALUE[%f]" % (time, id, value)

def timeseries_range(id, start, end):
    #print "QUERY DATUM_ID[%d] FROM START[%s] TO END[%s]" % (id, start, end)
    conn = get_db_connection(id)
    query = "SELECT datetime, value FROM timeseries WHERE datetime > ? AND datetime < ?"
    ans = conn.read(query, (date_as_string(start), date_as_string(end)))
    return [dict(datetime=x[0], value=x[1]) for x in ans]

